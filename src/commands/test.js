const {Configuration} = require('../configuration/Configuration')

module.exports.command = ['test']
module.exports.aliases = ['verify', 'validate', 'check', 'configtest', 'lint']
module.exports.desc = 'Check the configuration for problems'
module.exports.builder = {
  config: {
    type: 'string',
    describe: 'Configuration file'
  }
}

module.exports.handler = async (argv) => {
  await new Configuration(argv).load()
}
