const {join, resolve, dirname} = require('path')
const debug = require('debug')
const {fileExists} = require('../helpers/fileExists')
const {directoryExists} = require('../helpers/directoryExists')
const defaultsDeep = require('lodash.defaultsdeep')
const cloneDeep = require('lodash.clonedeep')
const {defaultOptions} = require('./default/options')
const {defaultHost} = require('./default/host')
const {ConfigurationValidator} = require('@http2/configuration')
const {setupTLS} = require('../helpers/setupTLS')
const userHome = require('user-home')

module.exports.Configuration = class Configuration {
  constructor (argv) {
    this.argv = argv
    this.log = debug(process.title)
  }

  async load ({generateCertificate} = {}) {
    const argv = this.argv
    const cwd = process.cwd()

    let userOptionsFilepath
    const fallbackConfigFilepath = resolve(cwd, `${process.title}.config.js`)
    let userOptions
    if (argv.config !== undefined) {
      userOptionsFilepath = resolve(cwd, argv.config)
      userOptions = require(userOptionsFilepath)
      delete require.cache[userOptionsFilepath]
    } else if (fileExists(fallbackConfigFilepath)) {
      userOptionsFilepath = fallbackConfigFilepath
      userOptions = require(userOptionsFilepath)
      delete require.cache[userOptionsFilepath]
    } else {
      userOptions = {}
    }

    const options = defaultsDeep(userOptions, defaultOptions)

    if (options.hosts.length === 0) {
      options.hosts.push(cloneDeep(defaultHost))
    }

    for (const host of options.hosts) {
      defaultsDeep(host, defaultHost)

      if (host.root === '') {
        const publicDirectory = join(cwd, 'public')
        host.root = argv.root ? resolve(argv.root)
          : directoryExists(publicDirectory) ? publicDirectory
          : cwd
      } else {
        host.root = resolve(dirname(userOptionsFilepath) || cwd, host.root)
      }

      if (typeof host.fallback === 'string') {
        host.fallback = {200: host.fallback}
      }
      for (const statusCode of Object.keys(host.fallback)) {
        host.fallback[statusCode] = resolve(
          host.root,
          host.fallback[statusCode].replace(/^\/(.*)/, '$1')
        )
      }
    }

    if (process.env.PORT !== undefined) {
      const httpsPort = Number(process.env.PORT)
      options.https.port = httpsPort
      options.http.port = httpsPort - (httpsPort % 1000) + 80
    }

    if (argv.loglevel !== undefined) {
      options.log.level = String(argv.loglevel)
    }

    if (options.acme.redirect.endsWith('/')) {
      options.acme.redirect = options.acme.redirect.replace(/\/+$/g, '')
    }

    if (options.acme.webroot !== '') {
      options.acme.webroot = resolve(cwd, options.acme.webroot)
    }

    if (options.acme.store !== '') {
      options.acme.store = resolve(process.cwd(), options.acme.store)
    }

    if (options.https.key === '' && options.https.cert === '') {
      const key = join(userHome, `.${process.title}`, 'key.pem')
      const cert = join(userHome, `.${process.title}`, 'cert.pem')
      if (fileExists(key) && fileExists(cert)) {
        options.https.key = key
        options.https.cert = cert
      } else if (generateCertificate) {
        console.log(
          '🔐 Generating a private TLS certificate.\n' +
          '   Confirm to add as a trusted certificate to your key chain.'
        )
        await setupTLS(key, cert)
        options.https.key = key
        options.https.cert = cert
      }
    }

    new ConfigurationValidator().validate(options)

    return options
  }
}
