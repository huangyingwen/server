const mime = require('mime')
const compressible = require('compressible')
const {checkImmutable} = require('./checkImmutable')

const ONE_YEAR = 31536e3

const compressors = [
  {extension: '.br', encoding: 'br'},
  {extension: '.gz', encoding: 'gzip'},
  {extension: '.deflate', encoding: 'deflate'}
]

module.exports.buildHeaders = function buildHeaders (
  sourceFile,
  immutablePatterns,
  supportedEncodings,
  fileIndex
) {
  const headers = {}
  const type = mime.lookup(sourceFile.absolute)
  const charset = mime.charsets.lookup(type)
  headers['content-type'] = charset
    ? `${type}; charset=${charset}`
    : type

  const isImmutable = checkImmutable(sourceFile.absolute, immutablePatterns)
  headers['cache-control'] = isImmutable
    ? `public, max-age=${ONE_YEAR}, immutable`
    : 'public, must-revalidate'

  if (compressible(type)) {
    for (const {extension, encoding} of compressors) {
      const variant = sourceFile.absolute + extension
      if (fileIndex.absolute.has(variant) &&
        supportedEncodings.includes(encoding)
      ) {
        headers['content-encoding'] = encoding
        return {headers, resolved: fileIndex.absolute.get(variant)}
      }
    }
  }
  return {headers, resolved: sourceFile}
}
