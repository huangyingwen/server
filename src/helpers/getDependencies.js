const micromatch = require('micromatch')

function isUncompressed (absolute) {
  return !absolute.endsWith('.gz') &&
    !absolute.endsWith('.br') &&
    !absolute.endsWith('.deflate')
}

module.exports.getDependencies =
function getDependencies (sourceFile, manifest, fileIndex) {
  const filepaths = Array
    .from(fileIndex.relative.keys())
    .filter(isUncompressed)
  const dependencies = new Set()
  const priorities = new Map()
  trace(sourceFile, manifest, fileIndex, filepaths, dependencies, priorities)
  dependencies.delete(sourceFile)
  return {dependencies, priorities}
}

function trace (sourceFile, manifest, fileIndex, filepaths, dependencies, priorities) {
  for (const rule of manifest) {
    for (const get of rule.get) {
      if (micromatch.any(sourceFile.relative, get.glob)) {
        for (const push of rule.push) {
          const deps = micromatch(filepaths, push.glob)
          for (const dep of deps) {
            const depFile = fileIndex.relative.get(dep)
            if (!dependencies.has(depFile)) {
              dependencies.add(depFile)
              priorities.set(depFile, push.priority)
              trace(depFile, manifest, fileIndex, filepaths, dependencies, priorities)
            }
          }
        }
      }
    }
  }
}
