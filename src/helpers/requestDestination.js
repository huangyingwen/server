const mime = require('mime')

const APPLICATION_OFFSET = 'application/'.length

module.exports.requestDestination = (filepath) => {
  const length = filepath.length

  if (filepath.charAt(length - 3) === '.' &&
    (filepath.charAt(length - 2) === 'j' || filepath.charAt(length - 2) === 'J') &&
    (filepath.charAt(length - 1) === 's' || filepath.charAt(length - 1) === 'S')
  ) return 'script'

  if (filepath.charAt(length - 4) === '.') {
    if (
      (filepath.charAt(length - 3) === 'c' || filepath.charAt(length - 3) === 'C') &&
      (filepath.charAt(length - 2) === 's' || filepath.charAt(length - 2) === 'S') &&
      (filepath.charAt(length - 1) === 's' || filepath.charAt(length - 1) === 'S')
    ) return 'style'

    switch (filepath.substr(length - 3, 3).toLowerCase()) {
      case 'png':
      case 'svg':
      case 'jpg':
      case 'gif':
      case 'ico':
        return 'image'
      case 'ttf':
      case 'eot':
      case 'otf':
        return 'font'
      case 'vtt':
        return 'track'
    }
  }

  if (filepath.charAt(length - 5) === '.') {
    switch (filepath.substr(length - 4, 4).toLowerCase()) {
      case 'webp':
      case 'jpeg':
        return 'image'
      case 'woff':
        return 'font'
    }
  }

  if (filepath.endsWith('.woff2')) return 'font'

  const type = mime.lookup(filepath)
  switch (type.slice(0, 5)) {
    case 'image': return 'image'
    case 'audio': return 'audio'
    case 'video': return 'video'
    case 'font/': return 'font'
  }

  if (type.includes('font', APPLICATION_OFFSET)) return 'font'

  return 'fetch'
}

// This is the original implementation.
// Its code is easier to read but runs 3-6x slower.

module.exports._requestDestination = (filepath) => {
  if (filepath.toLowerCase().endsWith('.js')) return 'script'
  if (filepath.toLowerCase().endsWith('.css')) return 'style'
  if (filepath.toLowerCase().endsWith('.vtt')) return 'track'

  const type = mime.lookup(filepath)

  if (type.startsWith('image')) return 'image'
  if (type.startsWith('audio')) return 'audio'
  if (type.startsWith('video')) return 'video'
  if (type.includes('font')) return 'font'

  return 'fetch'
}
