const {unlink, rmdir} = require('fs')
const {dirname, join} = require('path')
const {platform} = require('os')
const {spawnSync} = require('child_process')
const {exec} = require('child-process-promise')
const {promisify} = require('util')
const shellescape = require('shell-escape')
const mkdirp = require('mkdirp')
const commandExists = require('command-exists')
const userHome = require('user-home')
const {directoryExists} = require('./directoryExists')

const commonName = 'localhost'

const subjectAltName = [
  'DNS:localhost',
  'DNS:*.localhost',
  'IP:127.0.0.1',
  'IP:0.0.0.0',
  'IP:::1'
].join()

async function generateLocalhostPair (key, cert) {
  spawnSync('openssl', [
    // https://wiki.openssl.org/index.php/Manual:Req(1)
    'req',

    // generates a new certificate
    '-new',

    // outputs a self signed certificate instead of a certificate request
    '-x509',

    // the number of days to certify the certificate for
    '-days', '365',

    // private key will not be encrypted
    '-nodes',

    // RSA is widely supported...
    // '-newkey', 'rsa:2048',

    // ... but ECC is more efficient in bandwidth/CPU/RAM.
    // The prime256v1 curve is enabled by default in Node.js.
    // The secp384r1 curve is disabled in Node 8.6+ by default.
    // Both are widely supported in browsers recommended by NIST.
    // To be replaced by x25519 soon. See: nodejs/node#1495
    '-newkey', 'ec:<(openssl ecparam -name prime256v1)',

    // the message digest to sign the request with
    '-sha256',

    '-keyout', shellescape([key]),
    '-out', shellescape([cert]),

    // origins covered by this certificate
    '-subj', `/CN=${commonName}`,
    '-extensions', 'SAN',
    '-reqexts', 'SAN',
    '-config', `<(cat /etc/ssl/openssl.cnf <(printf "\\n[SAN]\\nsubjectAltName=${subjectAltName}"))`
  ], {shell: '/bin/bash'})
}

async function getDefaultKeychain () {
  const command = 'security default-keychain'
  const {stdout: keychain} = await exec(command)
  return keychain.trim().replace(/^"(.+)"$/, '$1')
}

async function addTrustedCertificate (keychain, cert) {
  const command = shellescape([
    'security',
    '-v', 'add-trusted-cert',
    '-r', 'trustRoot',
    '-p', 'ssl',
    '-k', keychain,
    cert
  ])
  await exec(command)
}

async function verifyNssdb () {
  const db = join(userHome, '.pki', 'nssdb')
  return directoryExists(db)
}

async function verifyCertutil () {
  if (!await promisify(commandExists)('certutil')) {
    throw new Error('certutil not found')
  }
}

// Delete:
// certutil -D -d sql:${HOME}/.pki/nssdb -n localhost

// List all:
// certutil -L -d sql:${HOME}/.pki/nssdb

async function certutilAddCertificate (cert) {
  const db = join(userHome, '.pki', 'nssdb')
  const command = shellescape([
    'certutil',
    '-A',
    '-d', `sql:${db}`,
    '-n', 'localhost',
    '-i', cert,
    '-t', 'C,,'
  ])
  await exec(command)
}

module.exports.setupTLS = async (key, cert) => {
  switch (platform()) {
    case 'darwin':
      try {
        await Promise.all([
          promisify(mkdirp)(dirname(key)),
          promisify(mkdirp)(dirname(cert))
        ])
        const keychain = await getDefaultKeychain()
        await generateLocalhostPair(key, cert)
        await addTrustedCertificate(keychain, cert)
      } catch (error) {
        try {
          await promisify(unlink)(key)
          await promisify(unlink)(cert)
          await promisify(rmdir)(dirname(key))
          await promisify(rmdir)(dirname(cert))
        } catch (error) {}
        throw new Error('Failed to set up SSL certificate')
      }
      break
    case 'linux':
      try {
        await Promise.all([
          promisify(mkdirp)(dirname(key)),
          promisify(mkdirp)(dirname(cert))
        ])
        await generateLocalhostPair(key, cert)
        if (await verifyNssdb()) {
          await verifyCertutil()
          await certutilAddCertificate(cert)
        } else {
          console.warn('Missing NSS database')
        }
      } catch (error) {
        try {
          await promisify(unlink)(key)
          await promisify(unlink)(cert)
          await promisify(rmdir)(dirname(key))
          await promisify(rmdir)(dirname(cert))
        } catch (error) {}
        throw error // new Error(`Failed to set up SSL certificate`)
      }
      break
    default:
      throw new Error('Generating certificates on this platform is not supported.')
  }
}
