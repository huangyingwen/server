module.exports.allowCors = () => {
  return (request, response, next) => {
    const allowed = request.options.accessControl.allowOrigin
    if (allowed) {
      response.setHeader('access-control-allow-origin', allowed)
    }
    next()
  }
}
