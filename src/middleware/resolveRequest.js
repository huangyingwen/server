const {format} = require('url')
const {extname} = require('path')
const parseUrl = require('parseurl')
const {getHost} = require('../helpers/getHost')
const {NotFound} = require('http-errors')
const accepts = require('accepts')

const PERMANENT_REDIRECT = 308

module.exports.resolveRequest = () => (request, response, next) => {
  const url = parseUrl(request)
  const pathname = decodeURIComponent(url.pathname)

  const {fileIndex} = request
  if (fileIndex.pathnames.has(pathname)) {
    request.resolved = fileIndex.pathnames.get(pathname)
    return next()
  } else {
    const {trailingSlash} = request.options.directories
    const hasTrailingSlash = pathname.endsWith('/')

    if (trailingSlash === 'always' && !hasTrailingSlash) {
      const indexHtml = `${pathname}/`
      if (fileIndex.pathnames.has(indexHtml)) {
        url.protocol = 'https'
        url.host = getHost(request)
        url.pathname = indexHtml
        response.writeHead(PERMANENT_REDIRECT, {location: format(url)})
        response.end()
        return
      }
    }

    if (trailingSlash === 'never' && hasTrailingSlash) {
      const indexHtml = pathname.slice(0, -1)
      if (fileIndex.pathnames.has(indexHtml)) {
        url.protocol = 'https'
        url.host = getHost(request)
        url.pathname = indexHtml
        response.writeHead(PERMANENT_REDIRECT, {location: format(url)})
        response.end()
        return
      }
    }

    const fallback = request.options.fallback['200']
    if (fileIndex.absolute.has(fallback)) {
      const extension = extname(pathname)
      switch (extension) {
        case '':
        case '.html':
        case '.htm':
          const accepted = accepts(request)
          if (accepted.type('text/html')) {
            request.resolved = fileIndex.absolute.get(fallback)
            return next()
          }
          break
        default:
      }
    }

    return next(new NotFound())
  }
}
