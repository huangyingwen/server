const {promisify} = require('util')
const {createServer} = require('http')
const parseUrl = require('parseurl')
const send = require('send')
const {untilBefore} = require('until-before')
const {getHost} = require('./helpers/getHost')

const MOVED_PERMANENTLY = 301
const PERMANENT_REDIRECT = 308
const MISDIRECTED_REQUEST = 421
const INTERNAL_SERVER_ERROR = 500

const ACME_PREFIX = '/.well-known/acme-challenge/'

const DEFAULT_PORT_HTTPS = 443

module.exports.redirect = async (options) => {
  function requestListener (request, response) {
    const hostname = untilBefore.call(getHost(request), ':')
    const {pathname} = parseUrl(request)

    console.log(`HTTP/${request.httpVersion} ${request.method} ${request.url}`)

    if (pathname.startsWith(ACME_PREFIX)) {
      if (options.acme.redirect.length > 0) {
        const location = options.acme.redirect + pathname
        response.writeHead(MOVED_PERMANENTLY, {location})
        response.end()
        return
      } else if (options.acme.webroot.length > 0) {
        const {pathname} = parseUrl(request)
        const sendOptions = {
          root: options.acme.webroot,
          etag: false,
          lastModified: false
        }
        send(request, pathname, sendOptions)
          .on('error', (error) => {
            response.statusCode = error.status || INTERNAL_SERVER_ERROR
            response.end()
          })
          .pipe(response)
        return
      }
    }

    if (hostname === '') {
      response.writeHead(MISDIRECTED_REQUEST)
      response.end()
    } else {
      const location = options.http.to === DEFAULT_PORT_HTTPS
        ? `https://${hostname}${request.url}`
        : `https://${hostname}:${options.http.to}${request.url}`
      response.writeHead(PERMANENT_REDIRECT, {location})
      response.end()
    }
  }
  const server = createServer(requestListener)
  await promisify(server.listen.bind(server))(options.http.from)
  return server
}
