require('hard-rejection/register')
const {server} = require('./server')
const {sync: readPkgUp} = require('read-pkg-up')

const [name] = Object.keys(readPkgUp({cwd: __dirname}).pkg.bin)
process.title = `${name}-worker`

process.once('message', ({options, files}) => {
  const app = server(options, files)
  app.listen(options.https.port)
})
