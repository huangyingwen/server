module.exports = {
  workers: {
    count: 'max_physical_cpu_cores / 2'
  },
  acme: {
    redirect: 'http://localhost:12345/',
    store: 'test/fixtures/acme/store',
    webroot: 'test/fixtures/acme/webroot'
  },
  hosts: [
    {
      domain: 'localhost',
      fallback: {
        404: '/404.html'
      },
      cacheControl: {
        immutable: [
          'hex',
          'emoji'
        ]
      },
      manifest: [
        {
          glob: '**/*.html',
          push: [
            {
              glob: [
                '**/*',
                '!**/*.{map,html}',
                '!**/service-worker/**/*'
              ],
              priority: 256
            }
          ]
        }
      ]
    },
    {
      domain: '127.0.0.1',
      fallback: {
        200: '/200.html'
      }
    }
  ]
}
