const test = require('ava')
const {getDependencies} = require('../src/helpers/getDependencies')
const {normaliseManifest} = require('../src/helpers/normaliseManifest')
const {indexFiles} = require('../src/helpers/indexFiles')

const PRIORITY_DEFAULT = 16
const PRIORITY_HIGH = 256
const PRIORITY_LOW = 1

const fileA = '/bla/index.html'
const fileB = '/bla/foo.js'
const fileC = '/bla/bar.js'
const fileD = '/bla/foo.js.map'
const fileE = '/bla/foo.js.gz'
const fileF = '/bla/foo.js.br'
const fileG = '/bla/foo.svg'
const fileH = '/bla/foo.css'

const root = '/bla'
const index = [fileA, fileB, fileC, fileD, fileE, fileF, fileG, fileH]
const trailingSlashAlways = 'always'

const fileIndex = indexFiles(root, index, trailingSlashAlways)

const fixtures = [
  {
    scenario: 'Nested dependencies',
    given: {
      fileIndex,
      sourceFile: fileIndex.absolute.get(fileA),
      manifest: [
        {glob: '/*.html', push: '/foo.js'},
        {glob: '/foo.js', push: ['**/*', '!**/*.map']}
      ]
    },
    expected: {
      dependencies: new Set([
        fileIndex.absolute.get(fileB),
        fileIndex.absolute.get(fileC),
        fileIndex.absolute.get(fileG),
        fileIndex.absolute.get(fileH)
      ])
    }
  },
  {
    scenario: 'Custom priority',
    given: {
      fileIndex,
      sourceFile: fileIndex.absolute.get(fileA),
      manifest: [
        {
          glob: '/index.html',
          push: [
            {glob: '**/*.js', priority: PRIORITY_HIGH},
            {glob: '**/*.css'},
            {glob: '**/*.svg', priority: PRIORITY_LOW}
          ]
        }
      ]
    },
    expected: {
      dependencies: new Set([
        fileIndex.absolute.get(fileB),
        fileIndex.absolute.get(fileC),
        fileIndex.absolute.get(fileH),
        fileIndex.absolute.get(fileG)
      ]),
      priorities: new Map([
        [fileIndex.absolute.get(fileB), PRIORITY_HIGH],
        [fileIndex.absolute.get(fileC), PRIORITY_HIGH],
        [fileIndex.absolute.get(fileH), PRIORITY_DEFAULT],
        [fileIndex.absolute.get(fileG), PRIORITY_LOW]
      ])
    }
  }
]

for (const {
    scenario,
    given: {sourceFile, manifest, fileIndex},
    expected
  } of fixtures
) {
  test(scenario, (t) => {
    const actual = getDependencies(
      sourceFile,
      normaliseManifest(manifest),
      fileIndex
    )
    if (expected.dependencies !== undefined) {
      t.deepEqual(actual.dependencies, expected.dependencies)
    }
    if (expected.priorities !== undefined) {
      t.deepEqual(actual.priorities, expected.priorities)
    }
  })
}
